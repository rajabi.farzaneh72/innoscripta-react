# FreshNews

Description of your project.

## News Aggregator Test Project with React.js

Before you begin, ensure you have met the following requirements:

- Docker: [Install Docker](https://docs.docker.com/get-docker/)

## Getting Started

To get a local copy up and running, follow these simple steps.

### Installation

1. Clone the repo

   ```bash
   git clone https://gitlab.com/rajabi.farzaneh72/innoscripta-react.git

2. Navigate to the project directory:

   ```bash
   cd innoscripta-react 

3. Build Docker Image

   ```bash 
   docker build -t innoscripta .

4. Run the Docker container:

    ```bash 
   docker run -p 3000:3000 innoscripta