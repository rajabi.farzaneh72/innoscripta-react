import './App.css';
import React from 'react';
import router from './router'
import { store } from './redux/store'
import { Provider } from 'react-redux'
import 'src/styles/css/_general-css-style.css'
import 'src/styles/scss/_general-scss-style.scss'
import { RouterProvider } from 'react-router-dom'
import GeneralLayout from 'src/modules/layout/components/general-layout'

function App() {
  return (
    <GeneralLayout>
      <Provider store={store}>
        <RouterProvider router={router} />
      </Provider>
    </GeneralLayout>
  );
}

export default App;
