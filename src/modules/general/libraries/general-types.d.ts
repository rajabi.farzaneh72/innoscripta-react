interface GeneralIconProps {
    className?: string
    onClick?: () => void
}

interface GeneralFilterProps<T> {
    criteriaKey: CriteriaKeyType
    onChange: (categoryKey: CriteriaKeyType, filterValue: T) => void
}





