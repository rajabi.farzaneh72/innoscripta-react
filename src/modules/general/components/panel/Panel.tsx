import React, { HTMLAttributes } from 'react'
import styles from './Panel.module.scss'

interface PanelProps extends HTMLAttributes<HTMLDivElement> {
    variant: 'large' | 'medium' | 'small'
}

export default function Panel({
    variant,
    ...props
}: PanelProps) {
    return (
        <div
            {...props}
            className={`${styles.root} ${props?.className ?? ''} variant_${variant}`}
        >
            {props.children}
        </div>
    )
}