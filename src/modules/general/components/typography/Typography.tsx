import React, { HTMLAttributes } from 'react'
import styles from './Typography.module.scss'

interface TypographyProps extends HTMLAttributes<HTMLParagraphElement> {
    variant: 'subtitle1' | 'subtitle2' | 'button1' | 'button2' | 'overline' | 'caption' | 'body1' | 'body2'
}

export default function Typography({
    variant,
    ...props
}: TypographyProps) {
    return (
        <p
            {...props}
            className={`${styles.root} ${props?.className ?? ''} variant_${variant}`}
        >
            {props.children}
        </p>
    )
}