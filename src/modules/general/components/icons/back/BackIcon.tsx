/* eslint-disable max-len */
import React from 'react';

export const BackIcon = ({
  className = '',
  onClick
}: GeneralIconProps) => (
  <svg
    onClick={onClick}
    className={className}
    width="800px"
    height="800px"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="Arrow / Arrow_Left_MD">
      <path id="Vector" d="M19 12H5M5 12L11 18M5 12L11 6" stroke="#000000" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
    </g>
  </svg>
);

export default BackIcon;