import React, { HTMLAttributes } from 'react'
import styles from './Button.module.scss'

interface ButtonProps extends HTMLAttributes<HTMLButtonElement> {
    variant?: 'contained' | 'outlined'
}

export default function Button({
    variant = 'contained',
    ...props
}: ButtonProps) {
    return (
        <button
            {...props}
            className={`${styles.root} ${props?.className ?? ''} variant_${variant}`}
        >
            {props.children}
        </button>
    )
}