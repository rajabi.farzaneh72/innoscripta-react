import DatePicker from 'react-datepicker';
import FilterPanel from '../filter-panel';
import styles from './FilterDate.module.scss'
import React, { useEffect, useState } from 'react'
import 'react-datepicker/dist/react-datepicker.css';
import { format } from 'date-fns';

export default function FilterDate({
    onChange,
    criteriaKey,
}: GeneralFilterProps<string>) {
    const
        [date, setDate] = useState<Date>(),

        handleChange = (newDate: Date) => {
            setDate(newDate)
        };

    useEffect(() => {
        if (onChange) {
            onChange(criteriaKey, date ? format(date, 'yyyy-MM-d') : '')
        }
    }, [date])
    return (
        <FilterPanel title={'Submission Date'}>
            <div className={styles.root}>
                <DatePicker
                    selected={date}
                    onChange={handleChange}
                    dateFormat="yyyy-MM-dd"
                    placeholderText="Select a date"
                />
            </div>
        </FilterPanel>
    )
}