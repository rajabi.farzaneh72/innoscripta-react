import styles from './FilterPanel.module.scss'
import React, { ReactNode, useState } from 'react'
import OpenIcon from 'src/modules/general/components/icons/open'
import Typography from 'src/modules/general/components/typography'
import RemoveIcon from 'src/modules/general/components/icons/remove'

interface FilterPanelProps {
    title: string,
    children: ReactNode
}

export default function FilterPanel({
    title,
    children
}: FilterPanelProps) {
    const
        [open, setOpen] = useState<boolean>(true),

        handleOpenToggle = () => {
            setOpen(prev => !prev)
        }

    return (
        <div className={styles.root}>
            <div className={styles.root__header}>
                <Typography variant={'subtitle2'}>
                    {title}
                </Typography>
                {open ? (
                    <RemoveIcon onClick={handleOpenToggle} />
                ) : (
                    <OpenIcon onClick={handleOpenToggle} />
                )}
            </div>
            <div className={`${styles.root__collapse} ${open ? styles.root__collapse__open : ''}`}>
                <div className={styles.root__collapse__container}>
                    {children}
                </div>
            </div>
        </div>
    )
}