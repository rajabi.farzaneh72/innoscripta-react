import React, { useEffect, useState } from 'react'
import styles from './FilterOptionsItem.module.scss'
import CheckedIcon from 'src/modules/general/components/icons/checked';

interface FilterOptionsItemProps {
    value: string
    disabled?:boolean
    onChange: (optionValue: string, optionStatus: boolean) => void
}

export default function FilterOptionsItem({
    value,
    onChange,
    disabled = false
}: FilterOptionsItemProps) {
    const
        [checked, setChecked] = useState<boolean>(disabled),

        handleCheckToggle = () => {
            if(!disabled){
                setChecked(prev => !prev)
            }
        };

    useEffect(() => {
        if (onChange&& !disabled) {
            onChange(value, checked)
        }
    }, [checked])

    return (
        <div
            className={styles.root}
            onClick={handleCheckToggle}
        >
            <div className={`${styles.root__icon} ${checked ? styles.root__icon__checked : ''}`}>
                {checked && <CheckedIcon />}
            </div>
            {value}
        </div>
    )
}