import FilterOption from './filter-options-item'
import styles from './FilterOptions.module.scss'
import React, { useEffect, useState } from 'react'
import FilterPanel from 'src/modules/general/components/filter/filter-panel'

interface FilterOptionsProps extends GeneralFilterProps<string[]> {
    title: string,
    options: string[],
}

export default function FilterOptions({
    title,
    options,
    onChange,
    criteriaKey,
}: FilterOptionsProps) {
    const
        [filterValue, setFilterValue] = useState<string[]>([] as string[]),

        handleChange = (optionValue: string, optionStatus: boolean) => {
            if (optionStatus) {
                setFilterValue([...filterValue, optionValue])
            } else {
                setFilterValue(filterValue.filter((item: string) => item !== optionValue))
            }
        }

    useEffect(() => {
        if (onChange) {
            onChange(criteriaKey, filterValue)
        }
    }, [filterValue,])

    return (
        <>
            {options && options.length > 0 && (
                <FilterPanel title={title}>
                    <div className={styles.root}>
                        {options.map((item: string, index: number) => {
                            return (
                                <React.Fragment key={`FilterItem_${index}`}>
                                    <FilterOption
                                        value={item}
                                        disabled={options.length===1}
                                        onChange={handleChange}
                                    />
                                </React.Fragment>
                            )
                        })}
                    </div>
                </FilterPanel>

            )}
        </>
    )
}