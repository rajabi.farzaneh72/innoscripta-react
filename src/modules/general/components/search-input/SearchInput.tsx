import React, { InputHTMLAttributes } from 'react'
import styles from './SearchInput.module.scss'
import SearchIcon from '../icons/search/SearchIcon'

export default function SearchInput({ ...props }: InputHTMLAttributes<HTMLInputElement>) {
    return (
        <div className={styles.root}>
            <SearchIcon className={styles.root__icon} />
            <input
                {...props}
                className={`${styles.root__input} ${props?.className ?? ''}`}
            />
        </div>
    )
}