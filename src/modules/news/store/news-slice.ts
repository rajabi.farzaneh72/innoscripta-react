import { createSlice } from '@reduxjs/toolkit'
import {
    initNewsState,
    newsResetHelper,
    updateNewsListHelper,
    updateNewsCriteriaHelper,
    updatePaginationHelper,
    updatePaginationFulfilled,
} from './news-helper'
import { initNewsListThunk } from './news-thunk';

export const newsSlice = createSlice({
    name: 'interests',
    initialState: initNewsState,
    reducers: {
        newsReset: newsResetHelper,
        updateNewsList: updateNewsListHelper,
        updateNewsCriteria: updateNewsCriteriaHelper,
        updatePagination: updatePaginationHelper
    },
    extraReducers: (builder) => {
        builder
            .addCase(initNewsListThunk.pending, (state) => {
                state.loading = true;
            })
            .addCase(initNewsListThunk.fulfilled, (state) => {
                updatePaginationFulfilled(state,state.newsList)
                state.loading = false;
            })
            .addCase(initNewsListThunk.rejected, (state) => {
                state.loading = false;
            });
    },
})

export const {
    newsReset,
    updateNewsList,
    updateNewsCriteria,
    updatePagination
} = newsSlice.actions

export default newsSlice.reducer