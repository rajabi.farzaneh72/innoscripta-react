import { PayloadAction } from '@reduxjs/toolkit';
import { InterestsItem } from 'src/modules/interests/libraries/interests-types';
import { Category, InterestsTypes } from 'src/modules/interests/libraries/interests-constants';
import {
    News,
    NewsCriteria,
    NewsState,
    UpdateNewsListPayload,
    UpdateObjectKeyWord
} from 'src/modules/news/libraries/news-types';
import { CriteriaKeyType, LIST_LIMIT, PaginationKeyType } from '../libraries/news-constants';

export const initNewsState: NewsState = {
    newsList: [],
    criteria: {
        source: [] as string[],
        category: [] as Category[],
        keyword: '',
        date: ''
    },
    loading: false,
    pagination:{
        currentPage:1,
        displayData:[],
        itemsPerPage:LIST_LIMIT,
        pageCount:null
    }
}

export const updatePaginationFulfilled = (state: NewsState, list:News[]) =>{
    const {
        currentPage,
        itemsPerPage,
    } = state.pagination

state.pagination.pageCount = Math.ceil(list.length / itemsPerPage);
state.pagination.displayData = list.slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage)
}

export const newsResetHelper = () => initNewsState;

export const updateNewsListHelper = (state: NewsState, action: PayloadAction<UpdateNewsListPayload>) => {
    const sortedNewsList = action.payload.items.sort((a, b) => (a.submitDate && b.submitDate ? new Date(b.submitDate).getTime() - new Date(a.submitDate).getTime() : 0));
    state.newsList = sortedNewsList;
}

export const updateNewsCriteriaHelper = (state: NewsState, action: PayloadAction<UpdateObjectKeyWord<CriteriaKeyType,string|string[]>>) => {
    state.criteria = Object.assign({}, state.criteria, { [action.payload.itemName]: action.payload.itemValue, });

}

export const updatePaginationHelper = (state: NewsState, action: PayloadAction<UpdateObjectKeyWord<PaginationKeyType,number>>) => {
    state.pagination = Object.assign({}, state.pagination, { [action.payload.itemName]: action.payload.itemValue, });
    updatePaginationFulfilled(state,state.newsList)
}

export const initCriteria = (criteria: NewsCriteria, interests: InterestsItem[]): NewsCriteria => {
    const
        interestsSources = interests.filter((item: InterestsItem) => item.key === InterestsTypes.source)[0].values,
        interestsCategories = interests.filter((item: InterestsItem) => item.key === InterestsTypes.category)[0].values;

    return ({
        ...criteria,
        source: criteria.source?.length === 0 ? interestsSources : criteria.source,
        category: criteria.category?.length === 0 ? interestsCategories : criteria.category
    } as NewsCriteria)
}

