import axios from 'axios';
import { NewsSource } from './news-api-utils';
import { MAX_FETCH_LENGTH } from 'src/modules/news/libraries/news-constants';
import { castGuardianNews, castNewYorkTimesCategory, castNewYorkTimesNews, castNewsApiNews, } from 'src/modules/news/libraries/news-utils';
import { GuardianNews, NewYorkTimesNews, News, NewsApiNews, NewsCriteria, ResponseEntity } from 'src/modules/news/libraries/news-types';

export class GuardianNewsSource extends NewsSource {

  private buildQueryParams(criteria: NewsCriteria): string {
    const
      formatParam = 'format=json',
      pageSizeParam = `&page-size=${MAX_FETCH_LENGTH}`,
      fieldsParam = '&show-fields=all',

      query = criteria.keyword ? `&q=${criteria.keyword}` : '',
      submitDate = criteria.date ? `&from-date=${criteria.date}` : '',
      apiKeyParam = `&api-key=${process.env.REACT_APP_GUARDIAN_API_KEY}`;

    let categoryParams = '';
    if (criteria.category && criteria.category.length > 0) {
      categoryParams = criteria.category.map(category => `section=${category.toLowerCase()}`).join('&');
    }

    return `${formatParam}${pageSizeParam}${fieldsParam}${query}${submitDate}${`&${categoryParams}`}${apiKeyParam}`;
  }


  private buildApiUrl(criteria: NewsCriteria): string {
    const
      baseUrl = 'https://content.guardianapis.com/search',
      queryParams = this.buildQueryParams(criteria);
    return `${baseUrl}?${queryParams}`;
  }

  protected async fetchData(criteria: NewsCriteria): Promise<News[] | undefined> {
    const apiUrl = this.buildApiUrl(criteria);

    try {
      const
        response: ResponseEntity<GuardianNews> = await axios.get(apiUrl),
        newsList = response?.data?.response?.results
          ? castGuardianNews(response?.data?.response?.results)
          : undefined;

      if (newsList) {
        return (newsList);
      }
    } catch (error) {
      console.error(`Error fetching data from Guardian:`, error);
    }
  }
}

export class NewYorkTimesNewsSource extends NewsSource {

  private buildQueryParams(criteria: NewsCriteria): string {
    const
      pageSizeParam = `&page-size=${MAX_FETCH_LENGTH}`,
      query = criteria.keyword ? `&q=${criteria.keyword}` : '',
      submitDate = criteria.date ? `&fq=pub_date:("${criteria.date}")` : '',
      apiKeyParam = `api-key=${process.env.REACT_APP_NEW_YORK_TIMES_API_KEY}`;

    let filterParams = '';

    if (criteria.category && criteria.category.length > 0) {
      const
        castedCategories = castNewYorkTimesCategory(criteria.category),
        categoryFilters = castedCategories.map(category => `"${category}"`);
      filterParams += `&fq=section_name:(${categoryFilters.join(',')})`;
    }

    return `${apiKeyParam}${pageSizeParam}${query}${submitDate}${filterParams}`;
  }

  private buildApiUrl(criteria: NewsCriteria): string {
    const
      baseUrl = 'https://api.nytimes.com/svc/search/v2/articlesearch.json',
      queryParams = this.buildQueryParams(criteria);
    return `${baseUrl}?${queryParams}`;
  }

  protected async fetchData(criteria: NewsCriteria): Promise<News[] | undefined> {
    const apiUrl = this.buildApiUrl(criteria);

    try {
      const
        response: ResponseEntity<NewYorkTimesNews> = await axios.get(apiUrl),
        newsList = response?.data?.response?.docs
          ? castNewYorkTimesNews(response.data.response.docs)
          : undefined;

      if (newsList) {
        return (newsList);
      }
    } catch (error) {
      console.error(`Error fetching data from New York Times:`, error);
    }
  }
}

export class NewsApiNewsSource extends NewsSource {

  private buildQueryParams(criteria: NewsCriteria): string {
    const
      query = `&q=${criteria.category && criteria.category.map(category => `${category.toLowerCase()}`).join(' OR ')}${criteria.keyword ? `AND ${criteria.keyword}` : ''}`,
      submitDate = criteria.date ? `&from=${criteria.date}` : '',
      apiKeyParam = `apiKey=${process.env.REACT_APP_NEWS_API_API_KEY}`,
      searchInParam = '&searchIn=title'

    return `${apiKeyParam}${searchInParam}${submitDate}${query}`;
  }


  private buildApiUrl(criteria: NewsCriteria): string {
    const
      baseUrl = 'https://newsapi.org/v2/everything',
      queryParams = this.buildQueryParams(criteria);
    return `${baseUrl}?${queryParams}`;
  }

  protected async fetchData(criteria: NewsCriteria): Promise<News[] | undefined> {
    const apiUrl = this.buildApiUrl(criteria);

    try {
      const
        response: ResponseEntity<NewsApiNews> = await axios.get(apiUrl),
        newsList = response?.data?.articles
          ? castNewsApiNews(response.data.articles)
          : undefined;

      if (newsList) {
        return (newsList);
      }
    } catch (error) {
      console.error(`Error fetching data from New York Times:`, error);
    }
  }
}
