import { News, NewsCriteria } from 'src/modules/news/libraries/news-types';

export abstract class NewsSource {
  protected abstract fetchData(criteria: NewsCriteria): Promise<News[] | undefined>;

  async initNewsList(criteria: NewsCriteria): Promise<News[] | undefined> {
    try {
      const resultPromises = await this.fetchData(criteria);
      return resultPromises;

    } catch (e) {
      return Promise.reject(e);
    }
  }
}