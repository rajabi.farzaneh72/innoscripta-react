import { initCriteria } from './news-helper';
import { updateNewsList } from './news-slice';
import { RootState } from '@/src/redux/store';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { News } from 'src/modules/news/libraries/news-types';
import { Source } from 'src/modules/interests/libraries/interests-constants';
import { GuardianNewsSource, NewYorkTimesNewsSource, NewsApiNewsSource } from './api/news-api';

export const initNewsListThunk = createAsyncThunk<void, void, { state: RootState }>(
    'news/initList',
    async (_, { getState, dispatch }) => {

        const criteria = initCriteria(getState().newsReducer.criteria, getState().interestsReducer.interests);
        const fetchPromises: Promise<void>[] = [];
        const allResponses: { items: News[], source: Source }[] = [];

        if (criteria.source) {
            for (const source of criteria.source) {
                let newsSource;
                switch (source) {
                    case Source.Guardian:
                        newsSource = new GuardianNewsSource();
                        break;
                    case Source.NewsApi:
                        newsSource = new NewsApiNewsSource();
                        break;
                    case Source.NewYorkTimes:
                        newsSource = new NewYorkTimesNewsSource();
                        break;
                    default:
                        break;
                }

                if (newsSource) {
                    const fetchPromise = (async () => {
                        try {
                            const response = await newsSource.initNewsList(criteria) || [];
                            allResponses.push({ items: response, source: source as Source });
                        } catch (e) {
                            console.error(`Error fetching news for ${source}:`, e);
                        }
                    })();

                    fetchPromises.push(fetchPromise);
                }
            }

            await Promise.all(fetchPromises);

            const combinedResponses = allResponses.reduce((accumulator, current) => {
                return { items: [...accumulator.items, ...current.items], source: current.source };
            }, { items: [], source: Source.Guardian }); 
            dispatch(updateNewsList(combinedResponses));
        }
    }
);
