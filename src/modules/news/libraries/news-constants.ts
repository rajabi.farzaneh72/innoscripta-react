export const NEWS_IMAGE_WIDTH = 400
export const NEWS_IMAGE_HEIGHT = 300
export const NEW_YORK_TIMES_URL = 'https://www.nytimes.com'

export enum FormatDateType {
    iso = 0,
    long = 1,
    short = 2
}

export enum CriteriaKeyType {
    Keyword = 'keyword',
    Date = 'date',
    Category = 'category',
    Source = 'source'
}

export const REQUEST_DELAY = 1000;

export const MAX_FETCH_LENGTH = 100

export const LIST_LIMIT = 6

export enum PaginationKeyType{
    PageCount= 'pageCount',
    ItemsPerPage = 'itemsPerPage',
    CurrentPage = 'currentPage',
    DisplayData = 'displayData'
}