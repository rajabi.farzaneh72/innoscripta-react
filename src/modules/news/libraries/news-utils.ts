import { FormatDateType, NEW_YORK_TIMES_URL } from './news-constants';
import { Source } from 'src/modules/interests/libraries/interests-constants';
import { InterestsItem } from 'src/modules/interests/libraries/interests-types';
import { GuardianNewsResult, NewYorkTimesNews, News, NewsApiNews } from './news-types';

export const castGuardianNews = (items: GuardianNewsResult[]): News[] | undefined => {
    let result;
    if (items && items.length > 0) {
        result = items.map((item: GuardianNewsResult) => {
            return ({
                title: item.fields.headline,
                source: Source.Guardian,
                category: item.sectionName,
                submitDate: item.fields.firstPublicationDate,
                imagePath: item.fields.thumbnail
            } as News)
        })
    }
    return result;
}

export const castNewYorkTimesNews = (items: NewYorkTimesNews[]): News[] | undefined => {
    let result;
    if (items && items.length > 0) {
        result = items.map((item: NewYorkTimesNews) => {
            return ({
                title: item.headline.main,
                source: Source.NewYorkTimes,
                category: item.section_name,
                submitDate: item.pub_date,
                imagePath: `${NEW_YORK_TIMES_URL}/${item.multimedia[0]?.url}`
            } as News)
        })
    }
    return result;
}

export const castNewsApiNews = (items: NewsApiNews[]): News[] | undefined => {
    let result;
    if (items && items.length > 0) {
        result = items.map((item: NewsApiNews) => {
            return ({
                title: item.title,
                source: Source.NewsApi,
                category: '',
                submitDate: item.publishedAt,
                imagePath: item.urlToImage ?? '/images/empty.jpg'
            } as News)
        })
    }
    return result;
}


export function dateFormat(
    type: FormatDateType,
    dateString: string | undefined,
    separator?: string,
): string {

    if (dateString) {
        const
            language = 'en-GB',
            originalDate = new Date(dateString),
            currentYear = originalDate.toLocaleString(language, { year: 'numeric', }),
            currentMonthLong = originalDate.toLocaleString(language, { month: 'long', }),
            currentMonthNumeric = originalDate.toLocaleString(language, { month: 'numeric', }).padStart(2, '0'),
            currentDay = originalDate.toLocaleString(language, { day: 'numeric', }).padStart(2, '0'),
            currentDayNoZero = originalDate.toLocaleString(language, { day: 'numeric', }).padStart(1, '0');

        switch (type) {
            case FormatDateType.long: /* like  2023 Nov 21 */
                return `${currentDayNoZero} ${currentMonthLong} ${currentYear}`;
            case FormatDateType.short: /* like  2023/11/21 */
                return `${currentYear}${separator}${currentMonthNumeric}${separator}${currentDay}`;
            default:
                return '';
        }
    }
    return '';
}

export const newYorkTimesCategoryMapping: Record<string, string> = {
    'sport': 'Sports',
    'film': 'Movies',
    'technology':'Technology',
    'food':'Food',
    'culture':"Culture",
    'fashion':'Fashion'
  };

  export const castNewYorkTimesCategory = (categories: string[]): string[] => {
    return categories.map(category => newYorkTimesCategoryMapping[category.toLowerCase()] || category);
  };

  export function checkEmptyInterests(interests:InterestsItem[]){
    let isEmpty = true;

    interests.map((step:InterestsItem)=> 
    {if(step.values.length > 0){

        isEmpty=false
    }})

    return isEmpty;
  }