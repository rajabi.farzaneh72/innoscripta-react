import React, { useEffect, useState } from 'react'
import styles from './NewsSearch.module.scss'
import SearchInput from 'src/modules/general/components/search-input'

export default function NewsSearch({
  onChange,
  criteriaKey
}: GeneralFilterProps<string>) {
  const [value, setValue] = useState<string>(''),

    handleChange = (newValue: string) => {
      setValue(newValue)
    };

  useEffect(() => {
    if (onChange) {
      onChange(criteriaKey, value)
    }
  }, [value])

  return (
    <div className={styles.root}>

      <SearchInput
        value={value}
        className={styles.root__input}
        placeholder={'Search ...'}
        onChange={(e) => handleChange(e.currentTarget.value)}
      />
    </div>
  )
}