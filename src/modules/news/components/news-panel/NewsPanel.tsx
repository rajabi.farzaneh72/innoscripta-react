import React, { useEffect } from 'react'
import styles from './NewsPanel.module.scss'
import { useNavigate } from 'react-router-dom'
import NewsList from 'src/modules/news/components/news-list'
import Panel from 'src/modules/general/components/panel/Panel'
import { useAppDispatch, useAppSelector } from 'src/redux/hooks'
import NewsSidebar from 'src/modules/news/components/news-sidebar'
import { initNewsListThunk } from 'src/modules/news/store/news-thunk'
import { HOME_URL } from 'src/modules/general/libraries/url-constants'
import { checkEmptyInterests } from 'src/modules/news/libraries/news-utils'

export default function NewsPanel() {
  const
    navigate = useNavigate(),
    dispatch = useAppDispatch(),
    interests = useAppSelector((state) => state.interestsReducer.interests);

  useEffect(() => {
    if (checkEmptyInterests(interests)) {
      navigate(HOME_URL);
    }else{
      dispatch(initNewsListThunk());
    }
  }, [])

  return (
    <Panel
      variant={'medium'}
      className={styles.root}
    >
      <NewsList />
      <NewsSidebar />
    </Panel>
  )
}