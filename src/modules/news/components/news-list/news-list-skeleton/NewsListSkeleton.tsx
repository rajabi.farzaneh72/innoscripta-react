import React from 'react'
import styles from './NewsListSkeleton.module.scss'
import { LIST_LIMIT } from 'src/modules/news/libraries/news-constants';
import NewsCardSkeleton from 'src/modules/news/components/news-card/news-card-skeleton';

export default function NewsListSkeleton() {

    return (
        <div className={styles.root}>
            {[...Array(LIST_LIMIT)].map(() => {
                return (
                    <React.Fragment key={`NewsItem_${Math.random()}`}>
                        <NewsCardSkeleton className={styles.root__card} />
                    </React.Fragment>
                )
            })}
        </div>
    )
}