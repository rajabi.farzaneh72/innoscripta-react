import React from 'react'
import styles from './NewsPanelListEmptyState.module.scss'
import Card from 'src/modules/general/components/card'

export default function NewsPanelListEmptyState() {
  return (
    <Card className={styles.root}>
        <img src={'/images/noNewsFound.png'}/>
    </Card>
  )
}