import React from 'react'
import styles from './NewsList.module.scss'
import ReactPaginate from 'react-paginate';
import NewsListSkeleton from './news-list-skeleton';
import NewsCard from 'src/modules/news/components/news-card';
import { News } from 'src/modules/news/libraries/news-types';
import NewsPanelListEmptyState from './news-list-empty-state';
import { useAppDispatch, useAppSelector } from 'src/redux/hooks';
import { updatePagination } from 'src/modules/news/store/news-slice';
import { PaginationKeyType } from 'src/modules/news/libraries/news-constants';

export default function NewsList() {
    const
        dispatch = useAppDispatch(),
        loading = useAppSelector((state) => state.newsReducer.loading),
        {
            displayData,
            pageCount,
        } = useAppSelector((state) => state.newsReducer.pagination),

        handlePageChange = (selectedPage: { selected: number }) => {
            dispatch(updatePagination({ itemName: PaginationKeyType.CurrentPage, itemValue: selectedPage.selected + 1 }))
        }

    return (
        <div className={`${styles.root} ${(displayData.length > 0 || loading) ? styles.root__fitContent : ''}`}>
            {loading ? (
                <NewsListSkeleton />
            ) : (
                <>
                    {displayData && displayData.length > 0 ? (
                        <>
                            {displayData.map((news: News) => {
                                return (
                                    <React.Fragment key={`NewsItem_${Math.random()}`}>
                                        <NewsCard
                                            item={news}
                                            classes={{ root: styles.root__card }}
                                        />
                                    </React.Fragment>
                                )
                            })}
                            <ReactPaginate
                                pageCount={pageCount || 0}
                                onPageChange={handlePageChange}
                                containerClassName="pagination"
                                activeClassName="active"
                            />
                        </>
                    ) : (
                        <NewsPanelListEmptyState />
                    )}
                </>
            )}
        </div >
    )
}