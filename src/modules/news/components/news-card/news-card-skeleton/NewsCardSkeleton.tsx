import React from 'react'
import styles from './NewsCardSkeleton.module.scss'
import Card from 'src/modules/general/components/card'

interface NewsCardSkeletonProps {
    className: string
}

export default function NewsCardSkeleton({
    className = ''
}: NewsCardSkeletonProps) {

    return (
        <Card className={`${styles.root} ${className}`}>
            <div className={styles.root__image} />
            <div className={styles.root__title} >
                <div className={styles.root__title__skeleton} />
            </div>
        </Card>
    )
}