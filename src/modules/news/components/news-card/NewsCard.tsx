import React from 'react'
import styles from './NewsCard.module.scss'
import Card from 'src/modules/general/components/card'
import { dateFormat } from 'src/modules/news/libraries/news-utils'
import { FormatDateType } from 'src/modules/news/libraries/news-constants'
import Typography from 'src/modules/general/components/typography/Typography'
import { News, NewsCardClassType } from 'src/modules/news/libraries/news-types'

interface NewsCardProps {
    item: News
    classes?: NewsCardClassType
}

export default function NewsCard({
    item,
    classes,
}: NewsCardProps) {

    return (
        <Card className={`${styles.root} ${classes?.root ?? ''}`}>
            <div className={`${styles.root__image} ${classes?.image ?? ''}`}>
                <img
                    alt={item.title}
                    src={item.imagePath ?? ''}
                />
            </div>
            <Typography
                variant={'overline'}
                className={`${styles.root__date} ${classes?.date ?? ''}`}
            >
                {dateFormat(FormatDateType.long, item.submitDate, '')}
            </Typography>
            <Typography
                variant={'subtitle1'}
                className={`${styles.root__title} ${classes?.title ?? ''}`}
            >
                {item.title}
            </Typography>
            <div className={styles.root__infoRow}>
                <Typography
                    variant={'overline'}
                    className={classes?.source ?? ''}
                >
                    {item.source}
                </Typography>
                <Typography
                    variant={'overline'}
                    className={classes?.source ?? ''}
                >
                    {item.category}
                </Typography>
            </div>
        </Card>
    )
}