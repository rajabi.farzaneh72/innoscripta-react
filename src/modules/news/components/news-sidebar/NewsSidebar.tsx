import NewsSearch from '../news-search'
import styles from './NewsSidebar.module.scss'
import React, { useEffect, useState } from 'react'
import Card from 'src/modules/general/components/card'
import { useAppDispatch, useAppSelector } from 'src/redux/hooks'
import Typography from 'src/modules/general/components/typography'
import FilterIcon from 'src/modules/general/components/icons/filter'
import { initNewsListThunk } from 'src/modules/news/store/news-thunk'
import { updateNewsCriteria } from 'src/modules/news/store/news-slice'
import FilterDate from 'src/modules/general/components/filter/filter-date'
import { CriteriaKeyType } from 'src/modules/news/libraries/news-constants'
import { InterestsItem } from 'src/modules/interests/libraries/interests-types'
import FilterOptions from 'src/modules/general/components/filter/filter-options'
import { InterestsTypes } from 'src/modules/interests/libraries/interests-constants'

export default function NewsSidebar() {
    const
        dispatch = useAppDispatch(),
        [open, setOpen] = useState<boolean>(false),
        state = useAppSelector((state) => state.newsReducer.criteria),
        interests = useAppSelector((state) => state.interestsReducer.interests),
        sourceOptions = interests.filter((item: InterestsItem) => item.key === InterestsTypes.source)[0].values || [] as string[],
        categoryOptions = interests.filter((item: InterestsItem) => item.key === InterestsTypes.category)[0].values || [] as string[],

        handleFilterChange = (criteriaKey: CriteriaKeyType, filterValue: string | string[]) => {
            dispatch(updateNewsCriteria({ itemName: criteriaKey, itemValue: filterValue }))
        },

        handleOpenStatus = (status: boolean) => {
            setOpen(status)
        };


    useEffect(() => {
        dispatch(initNewsListThunk());
    }, [state])

    return (
        <>
            <div
                onClick={() => handleOpenStatus(false)}
                className={`${styles.backDrop} ${open ? styles.backDrop__open : ''}`}
            />
            <FilterIcon
                className={styles.filterIcon}
                onClick={() => handleOpenStatus(true)}
            />
            <Card className={`${styles.root} ${open ? styles.root__open : ''}`}>
                <Typography
                    variant={'subtitle1'}
                    className={styles.root__title}
                >
                    Filters
                </Typography>

                <NewsSearch
                    onChange={handleFilterChange}
                    criteriaKey={CriteriaKeyType.Keyword}
                />

                <FilterOptions
                    title={'Sources'}
                    options={sourceOptions}
                    onChange={handleFilterChange}
                    criteriaKey={CriteriaKeyType.Source}
                />

                <FilterOptions
                    title={'Categories'}
                    options={categoryOptions}
                    onChange={handleFilterChange}
                    criteriaKey={CriteriaKeyType.Category}
                />

                <FilterDate
                    onChange={handleFilterChange}
                    criteriaKey={CriteriaKeyType.Date}
                />
            </Card>
        </>
    )
}