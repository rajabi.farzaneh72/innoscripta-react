const MenuData: MenuItemType[] = [
    {
        title: 'movie',
        link: '#'
    },
    {
        title: 'book',
        link: '#'
    },
    {
        title: 'art',
        link: '#'
    },
    {
        title: 'fashion',
        link: '#'
    },
    {
        title: 'food',
        link: '#'
    },
    {
        title: 'sport',
        link: '#'
    },
]

export default MenuData