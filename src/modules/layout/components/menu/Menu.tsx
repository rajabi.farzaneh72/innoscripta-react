import React from 'react';
import MenuItem from './menu-item';
import MenuData from './menu-data';
import styles from './Menu.module.scss';

interface MenuProps {
  className?: string
}

export default function Menu({ className = '', }: MenuProps) {
  return (
    <>
      {MenuData && MenuData.length > 0 && (
        <div className={`${styles.root} ${className}`}>
          {MenuData.map((item: MenuItemType, index: number) => {
            return (
              <React.Fragment key={`MenuItem_${index}_${Math.random()}`}>
                <MenuItem item={item} />
              </React.Fragment>
            );
          })}
        </div>
      )}
    </>

  );
}