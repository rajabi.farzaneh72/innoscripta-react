import React from 'react';
import styles from './MenuItem.module.scss';

interface MenuItemProps {
  item: MenuItemType
  className?: string
}

export default function MenuItem({
  item,
  className = '',
}: MenuItemProps) {
  return (
    <a
      href={item.link}
      className={`${styles.root} ${className}`}
    >
      {item.title}
    </a>
  );
}