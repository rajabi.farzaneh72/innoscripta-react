import React from 'react'
import Menu from '../menu';
import styles from './Header.module.scss';
import Button from 'src/modules/general/components/button';
import LogoIcon from 'src/modules/general/components/icons/logo';
import { HOME_URL } from 'src/modules/general/libraries/url-constants';

export default function Header({ className = '', }: {
  className?: string
}) {
  return (
    <div className={`${styles.root} ${className}`}>
      <div className={styles.root__container}>
        <a
          href={HOME_URL}
          className={styles.root__container__logo}
        >
          <LogoIcon className={styles.root__container__logo__svg} />
        </a>
        <Menu />
        <Button
          variant={'contained'}
          className={styles.root__container__button}
        >
          Login / Sign up
        </Button>
      </div>
    </div>
  );
}