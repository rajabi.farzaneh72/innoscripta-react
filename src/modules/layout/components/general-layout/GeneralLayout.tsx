import React, { ReactNode } from 'react';
import Header from '../header';
import Footer from '../footer';
import styles from './GeneralLayout.module.scss'

export default function GeneralLayout({ children, }: {
  children: ReactNode
}) {
  return (
    <div className={styles.root}>
      <div className={styles.root__children}>
        <Header />
        {children}
      </div>
      <Footer />
    </div>
  );
}