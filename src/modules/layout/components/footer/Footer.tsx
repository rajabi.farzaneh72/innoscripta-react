import React from 'react';
import styles from './Footer.module.scss';
import CopyrightIcon from 'src/modules/general/components/icons/copyright';

const date = new Date();

export default function Footer() {
  return (
    <div className={styles.root}>
      <CopyrightIcon />
      {`${date.getFullYear()}-${date.getFullYear() + 1} . FreshNews . All Rights Reserved .`}
    </div>
  );
}