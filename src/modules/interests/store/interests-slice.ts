import { createSlice } from '@reduxjs/toolkit'
import {
    initInterestsState,
    interestsResetHelper,
    updateCurrentStepHelper,
    updateInterestsHelper
} from './interests-helper'

export const interestsSlice = createSlice({
    name: 'interests',
    initialState: initInterestsState,
    reducers: {
        interestsReset: interestsResetHelper,
        updateInterests: updateInterestsHelper,
        updateCurrentStep: updateCurrentStepHelper
    },
})

export const {
    interestsReset,
    updateInterests,
    updateCurrentStep
} = interestsSlice.actions

export default interestsSlice.reducer