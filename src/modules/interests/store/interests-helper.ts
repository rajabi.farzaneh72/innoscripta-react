import { PayloadAction } from '@reduxjs/toolkit';
import { InterestsTypes } from 'src/modules/interests/libraries/interests-constants';
import {
    InterestsState,
    UpdateCurrentStepPayload,
    UpdateInterestsPayload
} from 'src/modules/interests/libraries/interests-types';

export const initInterestsState: InterestsState = {
    interests: [
        {
            key: InterestsTypes.category,
            minLength: 2,
            values: []
        },
        {
            key: InterestsTypes.source,
            minLength: 1,
            values: []
        },
        {
            key: InterestsTypes.author,
            minLength: 1,
            values: []
        }
    ],
    currentStep: 0
}

export const interestsResetHelper = () => initInterestsState;

export const updateInterestsHelper = (state: InterestsState, action: PayloadAction<UpdateInterestsPayload>) => {
    const updatedItemIndex = state.interests.findIndex((item) => item.key === action.payload.key);
    const currentValues = state.interests[updatedItemIndex].values;

    if (currentValues.includes(action.payload.value)) {
        state.interests[updatedItemIndex].values = currentValues.filter((item) => item !== action.payload.value);
    } else {
        currentValues.push(action.payload.value);
        state.interests[updatedItemIndex].values = currentValues;
    }
}

export const updateCurrentStepHelper = (state: InterestsState, action: PayloadAction<UpdateCurrentStepPayload>) => {
    state.currentStep = action.payload.step
}