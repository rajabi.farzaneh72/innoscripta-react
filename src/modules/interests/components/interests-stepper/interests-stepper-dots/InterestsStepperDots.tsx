import { RootState } from '@/src/redux/store'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import styles from './InterestsStepperDots.module.scss'
import BackIcon from 'src/modules/general/components/icons/back/BackIcon';
import { updateCurrentStep } from 'src/modules/interests/store/interests-slice';

export default function InterestsStepperDots() {
    const
        dispatch = useDispatch(),
        state = useSelector((state: RootState) => state.interestsReducer),
        length = state.interests.length,
        [isFirstStep, setIsFirstStep] = useState<boolean>(true),

        handleBackClick = () => {
            if (!isFirstStep) {
                dispatch(updateCurrentStep({ step: state.currentStep - 1 }))
            }
        }

    useEffect(() => {
        setIsFirstStep(state.currentStep === 0)
    }, [state])

    return (
        <>
            {length > 1 && (
                <div className={styles.root}>
                    <BackIcon
                        onClick={handleBackClick}
                        className={`${styles.root__back} ${isFirstStep ? styles.root__back__dative : ''}`}
                    />
                    {[...Array(length),].map((item: number, index: number) => {
                        return (
                            <React.Fragment key={`${item}_${Math.random()}`}>
                                <div className={`${styles.root__dot} ${index < state.currentStep ? styles.root__dot__fill : ''} ${index === state.currentStep ? styles.root__dot__active : ''}`} />
                            </React.Fragment>
                        )
                    })}
                </div>
            )}
        </>

    )
}