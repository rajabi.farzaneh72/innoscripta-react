import React from 'react'
import { RootState } from '@/src/redux/store'
import styles from './InterestsStep.module.scss'
import InterestsStepItem from './interests-step-item'
import { useDispatch, useSelector } from 'react-redux'
import { updateInterests } from 'src/modules/interests/store/interests-slice'
import { StepperOptionType, StepperType } from 'src/modules/interests/libraries/interests-types'

interface InterestsStepProps {
    step: StepperType
}

export default function InterestsStep({ step }: InterestsStepProps) {
    const
        dispatch = useDispatch(),
        currentStep = useSelector((state: RootState) => state.interestsReducer.currentStep),
        interests = useSelector((state: RootState) => state.interestsReducer.interests),
        stepIndex = interests.findIndex((item => item.key === step.key)),
        stepInterestsValues = interests.filter((item) => item.key === step.key)[0].values,

        handleClick = (item: StepperOptionType) => {
            dispatch(updateInterests({ value: item.title, key: step.key }))
        };

    return (
        <>
            {currentStep === stepIndex && (
                <div className={styles.root}>
                    {step.options && step.options.length > 0 && step.options.map((item: StepperOptionType) => {
                        return (
                            <React.Fragment key={`StepperItem_${Math.random()}`}>
                                <InterestsStepItem
                                    item={item}
                                    className={styles.root__item}
                                    onClick={() => handleClick(item)}
                                    selected={stepInterestsValues.includes(item.title)}
                                />
                            </React.Fragment>
                        )
                    })}
                </div>
            )}
        </>
    )
}