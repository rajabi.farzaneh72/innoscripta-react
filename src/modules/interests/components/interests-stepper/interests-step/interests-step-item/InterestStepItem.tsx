import React, { HTMLAttributes } from 'react'
import styles from './InterestStepItem.module.scss'
import Typography from 'src/modules/general/components/typography'
import CheckedIcon from 'src/modules/general/components/icons/checked'
import { StepperOptionType } from 'src/modules/interests/libraries/interests-types'

interface InterestsStepItemProps extends HTMLAttributes<HTMLDivElement> {
    item: StepperOptionType
    selected: boolean
}

export default function InterestStepItem({
    item,
    selected,
    ...props
}: InterestsStepItemProps) {

    return (
        <div
            {...props}
            className={`${styles.root} ${props?.className ?? ''} `}
            style={{ backgroundImage: `url(${item.imagePath})` }}
        >
            <div className={`${styles.root__container} ${selected ? styles.root__container__selected : ''}`}>
                {selected && (
                    <CheckedIcon className={styles.root__container__selected__icon} />
                )}
                <Typography
                    variant={'subtitle1'}
                    className={styles.root__container__title}
                >
                    {item.title}
                </Typography>
            </div>
        </div>
    )
}