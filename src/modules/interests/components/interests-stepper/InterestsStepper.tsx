import React from 'react'
import InterestsStep from './interests-step'
import Card from 'src/modules/general/components/card'
import Panel from 'src/modules/general/components/panel'
import InterestsStepperDots from './interests-stepper-dots'
import Typography from 'src/modules/general/components/typography'
import { StepperType } from 'src/modules/interests/libraries/interests-types'
import interestsOptions from 'src/modules/interests/components/interests-options'
import InterestsStepperButton from './interests-stepper-button/InterestsStepperButton'

export default function InterestsStepper() {
    return (
        <Panel variant={'small'}>
            {interestsOptions && interestsOptions.length > 0 && (
                <Card>
                    <InterestsStepperDots />
                    <h3>
                        What are you interested in?
                    </h3>
                    <Typography variant={'body1'}>
                        This will customize your news home feed
                    </Typography>
                    {interestsOptions.map((step: StepperType) => {
                        return (
                            <React.Fragment key={`StepperItem_${Math.random()}`}>
                                <InterestsStep step={step} />
                            </React.Fragment>
                        )
                    })}
                    <InterestsStepperButton />
                </Card>
            )}
        </Panel>
    )
}