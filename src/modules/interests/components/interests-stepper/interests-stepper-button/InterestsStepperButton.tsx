import { RootState } from '@/src/redux/store'
import { useNavigate } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import styles from './InterestsStepperButton.module.scss'
import Button from 'src/modules/general/components/button'
import { NEWS_URL } from 'src/modules/general/libraries/url-constants'
import { updateCurrentStep } from 'src/modules/interests/store/interests-slice'

export default function InterestsStepperDots() {
    const
        navigate = useNavigate(),
        dispatch = useDispatch(),
        state = useSelector((state: RootState) => state.interestsReducer),
        currentStep = state.interests[state.currentStep],
        [isLastStep, setIsLastStep] = useState<boolean>(false),
        [count, setCount] = useState<number>(state.interests[state.currentStep]?.minLength),

        handleClick = () => {            
            if (count === 0) {
                if (isLastStep) {
                    navigate(NEWS_URL);
                } else {
                    dispatch(updateCurrentStep({ step: state.currentStep + 1 }))
                    if (state.interests[state.currentStep + 1]) {
                        setCount(state.interests[state.currentStep + 1].minLength)
                    }
                }
            }
        };

    useEffect(() => {
        if (count >= 0) {
            const lengthDifference = currentStep?.minLength - currentStep?.values.length
            setCount(lengthDifference >= 0 ? lengthDifference : 0)
        }
    }, [state])

    useEffect(() => {
        setIsLastStep(state.currentStep === (state?.interests.length-1));
    }, [currentStep])

    useEffect(() => {
    }, [isLastStep])

    return (
        <Button
            onClick={handleClick}
            className={styles.root}
            aria-disabled={count > 0}
        >
            {count === 0 ? (
                <>
                    {isLastStep ? (
                        <>Meet your home feed</>
                    ) : (
                        <>Next Step</>
                    )}
                </>
            ) : (
                <>
                    {`Pick ${count} more`}
                </>
            )}
        </Button>
    )
}