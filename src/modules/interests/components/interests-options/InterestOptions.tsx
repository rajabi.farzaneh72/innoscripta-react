import { StepperType } from 'src/modules/interests/libraries/interests-types'
import { Author, Category, InterestsTypes, Source } from 'src/modules/interests/libraries/interests-constants'

const InterestOptionsData: StepperType[] = [
    {
        key: InterestsTypes.category,
        options: [
            {
                title: Category.Film,
                imagePath: '/images/categories/movie.jpg'
            },
            {
                title: Category.Technology,
                imagePath: '/images/categories/technology.jpg'
            },
            {
                title: Category.Sport,
                imagePath: '/images/categories/sport.jpg'
            },
            {
                title: Category.Culture,
                imagePath: '/images/categories/culture.jpg'
            },
            {
                title: Category.Fashion,
                imagePath: '/images/categories/fashion.jpg'
            },
            {
                title: Category.Food,
                imagePath: '/images/categories/food.jpg'
            }
        ]
    },
    {
        key: InterestsTypes.source,
        options: [
            {
                title: Source.Guardian,
                imagePath: '/images/categories/guardian.jpg'
            },
            {
                title: Source.NewsApi,
                imagePath: '/images/categories/newsapi.jpg'
            },
            {
                title: Source.NewYorkTimes,
                imagePath: '/images/categories/times.jpg'
            },
        ]
    },
    {
        key: InterestsTypes.author,
        options: [
            {
                title: Author.Benzinga,
                imagePath: '/images/categories/benzinga.jpg'
            },
            {
                title: Author.CBS,
                imagePath: '/images/categories/cbs.jpg'
            },
            {
                title: Author.NikkeiAsia,
                imagePath: '/images/categories/nikkei.jpg'
            },
        ]
    }
]

export default InterestOptionsData