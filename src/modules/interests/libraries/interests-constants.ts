export enum InterestsTypes {
    source = 1,
    category = 2,
    author = 3
}

export enum Category {
    Culture = 'Culture',
    Film = 'Film',
    Sport = 'Sport',
    Technology = 'Technology',
    Fashion = 'Fashion',
    Food = 'Food'
}

export enum Author {
    NikkeiAsia = 'Nikkei Asia',
    CBS = 'CBS',
    Benzinga = 'Benzinga',
}

export enum Source {
    Guardian = 'The Guardian',
    NewYorkTimes = 'New York Times',
    NewsApi = 'News Api',
}

export const allCategoryList = [
    Category.Culture,
    Category.Film,
    Category.Technology,
    Category.Fashion,
    Category.Food,
    Category.Sport
]
export const allSourceList = [
    Source.NewYorkTimes,
    Source.NewsApi,
    Source.Guardian
]

