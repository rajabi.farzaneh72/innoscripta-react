import { InterestsTypes } from './interests-constants'

interface StepperOptionType {
    imagePath: string
    title: string
}

interface StepperType {
    key: InterestsTypes
    options: StepperOptionType[]
}

interface InterestsItem {
    key: InterestsTypes
    minLength: number
    values: string[]
}

interface InterestsState {
    interests: InterestsItem[]
    currentStep: number
}

interface UpdateInterestsPayload {
    key: InterestsTypes
    value: string
}

interface UpdateCurrentStepPayload {
    step: number
}