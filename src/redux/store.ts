import { configureStore } from '@reduxjs/toolkit'
import newsSlice from 'src/modules/news/store/news-slice';
import interestsSlice from 'src/modules/interests/store/interests-slice'
import { setupListeners, } from '@reduxjs/toolkit/dist/query';

export const store = configureStore({
    reducer: {
        newsReducer: newsSlice,
        interestsReducer: interestsSlice,
    },
})

setupListeners(store.dispatch);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;