import React from 'react'
import NewsPanel from 'src/modules/news/components/news-panel';
import { NEWS_URL } from 'src/modules/general/libraries/url-constants';
import InterestsStepper from 'src/modules/interests/components/interests-stepper';
import { createBrowserRouter, createRoutesFromElements, Route } from 'react-router-dom';


const Router = createBrowserRouter(
    createRoutesFromElements(
        <Route path='/'>
            <Route index element={<InterestsStepper />}></Route>
            <Route path={NEWS_URL} element={<NewsPanel />}></Route>
        </Route>
    )
)

export default Router;